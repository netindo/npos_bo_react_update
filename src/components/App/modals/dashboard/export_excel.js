import React, { Component } from "react";
import { ModalToggle } from "redux/actions/modal.action";
import connect from "react-redux/es/connect/connect";
import { float, toDate, toExcel } from "../../../../helper";
import ExportCommon from "../../common/ExportCommon";
import { EXTENSION } from "../../../../redux/actions/_constants";
import moment from "moment";

class DashboardExportExcel extends Component {
	constructor(props) {
		super(props);
		this.printExcel = this.printExcel.bind(this);
	}

	handleHeader() {
		return ["VARIASI", "NAMA BARANG", "STOK AKHIR"];
	}

	handleContent() {
		let props = [];
		let data = this.props.data;
		if (data !== undefined) {
			if (data.length > 0) {
				for (let i = 0; i < data.length; i++) {
					let v = data[i];
					props.push([
						v.ukuran,
						v.nm_brg,
						float(v.stok),
					]);
				}
			}
		}
		return {
			body: props,
			footer: null,
		};
	}
	printExcel(param) {
		let content = this.handleContent();
		// let dataFooter = content.footer;
		// let footer = [[""], [""], ["TOTAL", "", "", "", "", dataFooter.diskon, dataFooter.ppn, "", "", "", "", dataFooter.sisa, dataFooter.qtyBeli, dataFooter.jumlahBeli]];
		toExcel("EXPORT BARANG TERLARIS", this.props.periode, this.handleHeader(), content.body, [], param);
		this.props.dispatch(ModalToggle(false));
	}
	render() {
		console.log("export",this.props.data)
		return <ExportCommon modalType="dashboardExportExcel" isCsv={true} isExcel={true} callbackCsv={() => this.printExcel(EXTENSION.CSV)} callbackExcel={() => this.printExcel(EXTENSION.XLXS)} />;
		// return <p>asd</p>
	}
}

const mapStateToProps = (state) => {
	return {
		isOpen: state.modalReducer,
		type: state.modalTypeReducer,
	};
};
export default connect(mapStateToProps)(DashboardExportExcel);
