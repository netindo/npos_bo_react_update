import React, { Component } from "react";
import { ModalBody, ModalHeader } from "reactstrap";
import connect from "react-redux/es/connect/connect";
import WrapperModal from "../_wrapper.modal";
import { ModalToggle } from "redux/actions/modal.action";
import { FetchReportDetail } from "redux/actions/purchase/receive/receive.action";
import HeaderDetailCommon from "../../common/HeaderDetailCommon";
import {
    getMargin,
    float,
    generateNo,
    noData,
    parseToRp,
    toDate, handleError, isEmptyOrUndefined, swalWithCallback, rmComma, toCurrency,
} from "../../../../helper";
import TableCommon from "../../common/TableCommon";
import {FetchHutang, FetchNotaHutang} from "../../../../redux/actions/hutang/hutang.action";
import {handlePost} from "../../../../redux/actions/handleHttp";
import {HEADERS} from "../../../../redux/actions/_constants";
import moment from "moment/moment";
import Select from "react-select";
class FormBayarSemuaHutang extends Component {
    constructor(props) {
        super(props);
        this.state={
            jumlah_bayar:this.props.data.sisa_hutang,
            jenis_pembayaran:'',
            keterangan:'-',
            tanggal_bayar:moment( new Date()).format("yyyy-MM-DD"),
        }
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggle(e) {
        e.preventDefault();
        const bool = !this.props.isOpen;
        this.props.dispatch(ModalToggle(bool));
    }

    handleChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    handleSelect(res){
        this.setState({jenis_pembayaran: res.value});
    }

    async handleSubmit(){
        const url = `hutang/bayar_semua`
        let obj=this.props.data;
        const {jumlah_bayar,jenis_pembayaran,keterangan,tanggal_bayar}=this.state;
        const {logo,username}=this.props.auth.user;
        if(!isEmptyOrUndefined(jumlah_bayar,'jumlah_bayar')) return;
        if(!isEmptyOrUndefined(jenis_pembayaran,'jenis pembayaran')) return;
        if(!isEmptyOrUndefined(keterangan,'keterangan')) return;
        if(!isEmptyOrUndefined(tanggal_bayar,'tanggal bayar')) return;
        Object.assign(obj,{jumlah_bayar:rmComma(jumlah_bayar),logo,username,...this.state});
        await handlePost(url,obj,(e)=>{
            this.props.dispatch(ModalToggle(false));
            swalWithCallback("pembayaran berhasil dilakukan",()=>{
                this.props.callback(e.status==='success');
            })
        })
    }
    render() {
        console.log("form bayar semua hutang",this.props.data)

        const data=this.props.data;
        const jns=[
            {value:"Tunai", label:"Tunai"},
            {value:"Transfer", label:"Transfer"},
        ];
        return (
            <WrapperModal
                isOpen={this.props.isOpen && this.props.type === "formBayarSemuaHutang"}
                size="lg"
            >
                <ModalHeader toggle={this.toggle}>{data.nama}
                </ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <label htmlFor="">Jumlah Bayar</label>
                        <input readOnly className="form-control" type="text" name="jumlah_bayar" value={toCurrency(this.state.jumlah_bayar)} onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label className="control-label font-12">Jenis Pembayaran</label>
                        <Select
                            name="jenis_pembayaran"
                            options={jns}
                            placeholder="Pilih Metode"
                            onChange={this.handleSelect}
                            value={jns.find(op => op.value === this.state.jenis_pembayaran)}
                        />
                    </div>
                    <div className="form-group">
                        <label>Tanggal Pembayaran</label>
                        <input type="date" name={"tanggal_bayar"} className="form-control" value={this.state.tanggal_bayar} onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Keterangan</label>
                        <input type="text" name={"keterangan"} className="form-control" value={this.state.keterangan} onChange={this.handleChange}/>
                    </div>
                    <button style={{float:"right"}} className="btn btn-primary" onClick={this.handleSubmit}>submit</button>
                </ModalBody>
            </WrapperModal>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth:state.auth,
        isOpen: state.modalReducer,
        type: state.modalTypeReducer,
    };
};
export default connect(mapStateToProps)(FormBayarSemuaHutang);
