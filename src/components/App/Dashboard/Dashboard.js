import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../Layout";
import Chart from "react-apexcharts";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import moment from "moment";
import Select from "react-select";
import { toRp } from "helper";
import { FetchStock } from "redux/actions/dashboard/dashboard.action";
import DateRangePicker from "react-bootstrap-daterangepicker";
import "bootstrap-daterangepicker/daterangepicker.css";
import Cookies from "js-cookie";

import socketIOClient from "socket.io-client";
import { HEADERS } from "redux/actions/_constants";
import {dateRange, getStorage, parseToRp, rangeDate, setStorage, toDate} from "../../../helper";
import Clock from "../common/clock";
import Preloader from "../../../Preloader";
import {handleGet} from "../../../redux/actions/handleHttp";
import TableCommon from "../common/TableCommon";
import DashboardExportExcel from "../modals/dashboard/export_excel";
import {ModalToggle, ModalType} from "../../../redux/actions/modal.action";
const socket = socketIOClient(HEADERS.URL);

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      top_by_variasi:[],
      startDate: localStorage.getItem("startDateDashboard") === null ? moment(new Date()).format("yyyy-MM-DD") : localStorage.getItem("startDateDashboard"),
      endDate: localStorage.getItem("endDateDashboard") === null ? moment(new Date()).format("yyyy-MM-DD") : localStorage.getItem("endDateDashboard"),
      grossSales: "0",
      wGrossSales: 110,
      netSales: "0",
      wNetSales: 110,
      trxNum: "0",
      wTrxNum: 110,
      avgTrx: "0",
      wAvgTrx: 110,
      exportTopItem:false,
      loadingExportTopItem:false,
      dataUangPenjualanBerdasarkanBulan:[],
      dataBulanPenjualanBerdasarkanBulan:[],
      topItemByVariasi:{
        current_page:1,total:0,data:[],per_page:10,from:0,last_page:30,offset:0,to:1
      },
      barangTidakLaku:{
        current_page:1,total:0,data:[],per_page:10,from:0,last_page:30,offset:0,to:1
      },
      dataExportTopItem:[],

      dataA: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
      dataB: [9, 7, 3, 5, 2, 5, 3, 9, 6, 5],
      dataC: [5, 3, 9, 3, 5, 2, 6, 5, 9, 7],
      dataD: [7, 3, 5, 2, 5, 3, 9, 6, 5, 9],

      location_data: [],
      location: "-",

      lokasi_sales: {
        options: {
          chart: {
            id: "basic-bar",
          },
          xaxis: {
            categories: [],
          },
        },
        series: [
          {
            name: "Bulan Lalu",
            data: [],
          },
          {
            name: "Bulan Sekarang",
            data: [],
          },
        ],
      },
      lokasi_tr: {
        options: {
          chart: {
            id: "basic-bar",
          },
          xaxis: {
            categories: [],
          },
        },
        series: [
          {
            name: "Bulan Lalu",
            data: [],
          },
          {
            name: "Bulan Sekarang",
            data: [],
          },
        ],
      },
      daily: {
        options: {
          chart: {
            id: "basic-bar",
          },
          xaxis: {
            categories: ["Monday", "Thuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
          },
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0"],
          dataLabels: {
            enabled: false,
          },
          plotOptions: {
            bar: {
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [0, 0, 0, 0, 0, 0, 0],
          },
        ],
      },
      hourly: {
        options: {
          chart: {
            type: "area",
          },
          xaxis: {
            categories: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: "smooth",
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          },
        ],
      },
      top_item_qty: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [],
          },
        ],
      },
      top_item_sale: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [],
          },
        ],
      },
      top_cat_qty: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [],
          },
        ],
      },
      top_cat_sale: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [],
          },
        ],
      },
      top_sp_qty: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            // name: "Bulan Lalu",
            data: [],
          },
        ],
      },
      top_sp_sale: {
        options: {
          colors: ["#F44336", "#E91E63", "#9C27B0", "#F44336", "#E91E63", "#9C27B0", "#9C27B0", "#9C27B0", "#F44336"],
          xaxis: {
            categories: [],
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: "45%",
              distributed: true,
            },
          },
          legend: {
            show: false,
          },
          dataLabels: {
            enabled: false,
          },
        },
        series: [
          {
            data: [],
          },
        ],
      },
      chartPenjualanBerdasarkanBulan:null
    };

    socket.on("refresh_dashboard", (data) => {
      this.refreshData();
    });
    socket.on("set_dashboard", (data) => {
      if (data.tenant === atob(atob(Cookies.get("tnt=")))) {
        console.log("top by variasi",data.top_sp_qty)
        this.setState({
          isLoading:false,
          top_by_variasi:data.top_by_variasi,
          grossSales: toRp(parseInt(data.header.penjualan, 10)),
          netSales: toRp(parseInt(data.header.net_sales, 10)),
          trxNum: data.header.transaksi,
          avgTrx: toRp(parseInt(data.header.avg, 10)),
          lokasi_sales: data.lokasi_sales,
          lokasi_tr: data.lokasi_tr,
          hourly: data.hourly,
          daily: data.daily,
          top_item_qty: data.top_item_qty,
          top_item_sale: data.top_item_sale,
          top_cat_qty: data.top_cat_qty,
          top_cat_sale: data.top_cat_sale,
          top_sp_qty: data.top_sp_qty,
          top_sp_sale: data.top_sp_sale,
        });
      }
    });



    this.handleSelect = this.handleSelect.bind(this);
    this.handleSelect2 = this.handleSelect2.bind(this);
    this.handleSelect3 = this.handleSelect3.bind(this);
    this.HandleChangeLokasi = this.HandleChangeLokasi.bind(this);
    this.handleExportTopItem = this.handleExportTopItem.bind(this);
    this.HandleBarangTidakLaku = this.HandleBarangTidakLaku.bind(this);
  }
  handleExportTopItem(){
    this.setState({loadingExportTopItem:true},()=>{
      let url=`site/get_top_item_by_variasi?page=1&perpage=100&datefrom=${this.state.startDate}&dateto=${this.state.endDate}&lokasi=${btoa(this.state.location)}`;
      // else url+=`&lokasi=${btoa('LK/0001')}`;
      handleGet(url,(res)=>{
        console.log(res.data.result.data);
        this.setState({exportTopItem:true,loadingExportTopItem:false,dataExportTopItem:res.data.result.data},()=>{
          this.props.dispatch(ModalToggle(true));
          this.props.dispatch(ModalType("dashboardExportExcel"));
        });
        // let topItemByVariasi=this.state.topItemByVariasi;
        // Object.assign(topItemByVariasi,res.data.result);
        // this.setState({topItemByVariasi});
      });
    })




  }

  componentDidMount() {
    this.props.dispatch(FetchStock());
    this.HandlePenjualanBerdasarkanBulan()
    this.HandleBarangTidakLaku(1);
    this.HandleTopItemByVariasi(1,this.state.location,this.state.startDate,this.state.endDate);
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.auth.user) {
      let lk = [];
      let loc = nextProps.auth.user.lokasi;
      if (loc !== undefined) {
        if (loc.length === 1) {
          this.setState({
            location: loc[0].kode,
          });
          this.refreshData(this.state.startDate, this.state.endDate, loc[0].kode);
        } else {
          lk.push({
            value: "-",
            label: "Semua Lokasi",
          });
        }
        loc.map((i) => {
          lk.push({
            value: i.kode,
            label: i.nama,
            // label: i.ukuran,
          });
          return null;
        });

        this.setState({
          location_data: lk,
          userid: nextProps.auth.user.id,
        });
      }
    }
  };

  refreshData(start = null, end = null, loc = null) {
    start=start !== null ? start : this.state.startDate;
    end= end !== null ? end : this.state.endDate;
    loc= loc !== null ? loc : this.state.location;
    socket.emit("get_dashboard", {
      datefrom: start !== null ? start : this.state.startDate,
      dateto: end !== null ? end : this.state.endDate,
      location: loc !== null ? loc : this.state.location,
      tenant: atob(Cookies.get("tnt=")),
    });
    // this.HandleTopItemByVariasi(1,loc,start,end);

    // this.setState({isLoading:false},()=>{
    //   console.log("refersh");
    //   console.log(start,this.state.startDate)
    //   console.log("refersh done");
    // });


  }

  componentWillMount() {
    this.refreshData();
    // this.props.dispatch(FetchStock());
  }

  componentWillUnmount() {
    localStorage.removeItem("startDateProduct");
    localStorage.removeItem("endDateDashboard");
  }

  onChange = (date) => this.setState({ date });

  handleSelect = (e, index) => {
    this.setState({ selectedIndex: index });
  };
  handleSelect2 = (e, index) => {
    this.setState({ selectedIndex: index });
  };
  handleSelect3 = (e, index) => {
    this.setState({ selectedIndex: index });
  };

  handleEvent = (event, picker) => {
    // end:  2020-07-02T16:59:59.999Z
    const awal = moment(picker.startDate._d).format("YYYY-MM-DD");
    const akhir = moment(picker.endDate._d).format("YYYY-MM-DD");
    this.setState({
      // isLoading:true,
      startDate: awal,
      endDate: akhir,
    },()=>{
      this.refreshData(awal, akhir, null)
    });
    // this.refreshData(awal, akhir, null);
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.refreshData();
  };

  HandleChangeLokasi(lk) {
    let err = Object.assign({}, this.state.error, {
      location: "",
    });
    this.setState({
      location: lk.value,
      error: err,
    });
    this.HandleTopItemByVariasi(1,lk.value,this.state.startDate,this.state.endDate);
    this.refreshData(null, null, lk.value);
  }

  HandleStock(e) {
    e.preventDefault();
    this.props.dispatch(FetchStock());
  }

  HandleTopItemByVariasi(page=1,lokasi="",datefrom="",dateto=""){
    let url=`site/get_top_item_by_variasi?page=${page}&datefrom=${datefrom}&dateto=${dateto}`;
    if(lokasi!=="" && lokasi!=="-") url+=`&lokasi=${btoa(lokasi)}`
    else url+=`&lokasi=${btoa('LK/0001')}`;
    handleGet(url,(res)=>{
      console.log("bew data",res);
      let topItemByVariasi=this.state.topItemByVariasi;
      Object.assign(topItemByVariasi,res.data.result);
      this.setState({topItemByVariasi});
    });
  }

  HandleBarangTidakLaku(page=1,){
    let url=`site/get_barang_tidak_laku?page=${page}&perpage=10`;
    handleGet(url,(res)=>{
      let newRes=[];
      let barangTidakLaku=this.state.barangTidakLaku;
      Object.assign(barangTidakLaku,{
        current_page:res.data.result.current_page,
        total:res.data.result.total,
        data:res.data.result.data,
        per_page:res.data.result.per_page,
        from:res.data.result.from,
        last_page:res.data.result.last_page,
        offset:res.data.result.offset,
        to:res.data.result.to,
      });
      this.setState({barangTidakLaku});
    });
  }
  HandlePenjualanBerdasarkanBulan(){
    let url=`site/get_penjualan_berdasarkan_bulan`;
    handleGet(url,(res)=>{
      const data=res.data.result;
      this.setState({chartPenjualanBerdasarkanBulan:data})
    });
  }


  render() {
      console.log("state",this.state.chartPenjualanBerdasarkanBulan);
    const {total,per_page,last_page,offset,data,from,to,current_page}=this.state.topItemByVariasi;
    const btTotal=this.state.barangTidakLaku.total;
    const btPerpage=this.state.barangTidakLaku.per_page;
    const btData=this.state.barangTidakLaku.data;
    const btCurrentPage=this.state.barangTidakLaku.current_page;
    return (
        <Layout page={this.state.isLoading?'Tunggu Sebentar':'Dashboard'}>
          {
            this.state.isLoading&&<Preloader/>

          }
          <div>
            <div className="row align-items-center">
              <div className="col-6">
                <div className="dashboard-header-title mb-3">
                  <h5 className="mb-0 font-weight-bold">Dashboard</h5>
                </div>
              </div>
              {/* Dashboard Info Area */}
              <div className="col-6">
                <div className="dashboard-infor-mation d-flex flex-wrap align-items-center mb-3">
                  <div className="dashboard-clock">
                    <div id="dashboardDate">{moment().format("dddd, Do MMM YYYY")}</div>
                    <Clock />
                  </div>
                  <div className="dashboard-btn-group d-flex align-items-center">
                    <button type="button" onClick={(e) => this.handleSubmit(e)} className="btn btn-primary ml-1 float-right">
                      <i className="fa fa-refresh"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>


            <div className="row  mb-3">
              {/* Dashboard Info Area */}
              <div className="col-md-2 col-sm-2 col-lg-2">
                <div className="form-group">

                  {dateRange(
                      (first, last, isActive) => {
                        const awal = moment(first).format("YYYY-MM-DD");
                        const akhir = moment(last).format("YYYY-MM-DD");
                        this.setState({
                          isLoading:true,
                          startDate: awal,
                          endDate: akhir,
                        }, () => {
                          this.refreshData(awal, akhir, null);
                          this.HandleTopItemByVariasi(1,this.state.location,awal,akhir);
                        });
                        // this.handleService()
                      },
                      `${toDate(this.state.startDate)} - ${toDate(this.state.endDate)}`,
                      "",true,false
                  )}

                  {/*<DateRangePicker ranges={rangeDate} alwaysShowCalendars={true} onEvent={this.handleEvent}>*/}
                  {/*  <input readOnly type="text" className="form-control" name="date_product" value={`${this.state.startDate} to ${this.state.endDate}`} style={{ padding: "9px", fontWeight: "bolder" }} />*/}
                  {/*</DateRangePicker>*/}
                </div>
              </div>
              <div className="col-md-2 col-sm-2 col-lg-2">
                <div className="form-group">
                  <Select
                      options={this.state.location_data}
                      placeholder="Pilih Lokasi"
                      defaultValue={{ label: "Select Location", value: "-" }}
                      onChange={this.HandleChangeLokasi}
                      value={this.state.location_data.find((op) => {
                        return op.value === this.state.location;
                      })}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-6 col-xl-4 box-margin">
                <div className="card">
                  <div className="card-header bg-transparent border-bottom-0">GROSS SALES</div>
                  <div className="card-body">
                    <div className="row justify-content-between" style={{ paddingLeft: 12, paddingRight: 12 }}>
                      <h2>
                        <i className="fa fa-area-chart text-primary"></i>
                      </h2>
                      <h2 style={{ paddingLeft: 5 }} className="font-20">
                        {this.state.grossSales}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-xl-4 box-margin">
                <div className="card">
                  <div className="card-header bg-transparent border-bottom-0">NET SALES</div>
                  <div className="card-body">
                    <div className="row justify-content-between" style={{ paddingLeft: 12, paddingRight: 12 }}>
                      <h2>
                        <i className="fa fa-bar-chart text-secondary"></i>
                      </h2>
                      <h2 style={{ paddingLeft: 5 }} className="font-20">
                        {this.state.netSales}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-xl-4 box-margin">
                <div className="card">
                  <div className="card-header bg-transparent border-bottom-0">NUMBER OF TRANSACTION</div>
                  <div className="card-body">
                    <div className="row justify-content-between" style={{ paddingLeft: 12, paddingRight: 12 }}>
                      <h2>
                        <i className="fa fa-line-chart text-success"></i>
                      </h2>
                      <h2 style={{ paddingLeft: 5 }} className="font-20">
                        {this.state.trxNum}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-6 box-margin">
                <div className="card">
                  <div className="card-header" style={{justifyContent:"space-between",display:"flex"}}>
                    <span>BARANG TERLARIS</span>
                    <button className="btn btn-primary" onClick={()=>this.handleExportTopItem()}>{this.state.loadingExportTopItem?'Loading ... ':'Export'}</button>
                  </div>
                  <div className="card-body">
                    <TableCommon
                        height="355px"
                        head={[
                          { label: "No", className: "text-center", width: "1%" },
                          { label: "Variasi", width: "1%" },
                          { label: "Nama Barang" },
                          { label: "Qty", width: "1%" },
                        ]}
                        meta={{
                          total: total>100?100:total,
                          current_page: current_page,
                          per_page: per_page,
                        }}
                        body={typeof data === "object" && data}
                        label={[
                          { label: "ukuran" },
                          { label: "nm_brg" },
                          { label: "qty",isCurrency:true },
                        ]}
                        current_page={current_page}
                        callbackPage={(e,i)=>{
                          this.HandleTopItemByVariasi(e,this.state.location,this.state.startDate,this.state.endDate)
                          console.log(e,i)
                        }}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-6 box-margin">
                <div className="card">
                  <div className="card-header" style={{justifyContent:"space-between",display:"flex"}}>
                    <span>BARANG KURANG LAKU 3 BULAN TERKAHIR</span>
                  </div>
                  <div className="card-body">
                    <TableCommon
                        height="355px"
                        head={[
                          { label: "No", className: "text-center", width: "1%" },
                          { label: "Variasi", width: "1%" },
                          { label: "Nama Barang" },
                          { label: "Qty", width: "1%" },
                          { label: "Tanggal Input", width: "1%" },
                        ]}
                        meta={{
                          total: btTotal,
                          current_page: btCurrentPage,
                          per_page: btPerpage,
                        }}
                        body={typeof btData === "object" && btData}
                        label={[
                          { label: "variasi" },
                          { label: "nm_brg" },
                          { label: "qty",isCurrency:true },
                          { label: "tgl_input",date:true },
                        ]}
                        current_page={btCurrentPage}
                        callbackPage={(e,i)=>{
                          this.HandleBarangTidakLaku(e)
                        }}
                    />
                  </div>
                </div>

                {/*<div className="card">*/}
                {/*  <div className="card-header">STOK BARANG HABIS</div>*/}
                {/*  <div className="card-body" style={{ height: "470px", overflowY: "auto" }}>*/}
                {/*    <table className="table table-noborder">*/}
                {/*      <thead>*/}
                {/*      <tr>*/}
                {/*        <th className="text-center middle nowrap" width="1%">No</th>*/}
                {/*        <th className="middle nowrap">Variasi</th>*/}
                {/*        <th className="middle nowrap"  width="1%">Nama Barang</th>*/}
                {/*        <th className="middle nowrap" width="1%">Stock</th>*/}
                {/*      </tr>*/}
                {/*      </thead>*/}
                {/*      <tbody>*/}
                {/*      {*/}
                {/*        this.props.stock.length>0?this.props.stock.map((v,i)=>{*/}
                {/*          return (*/}
                {/*              <tr key={i}>*/}
                {/*                <td className="text-center middle nowrap">{i+1}</td>*/}
                {/*                <td className="middle nowrap">{v.ukuran}</td>*/}
                {/*                <td className="middle nowrap">{v.nm_brg}</td>*/}

                {/*                <td className="text-right middle nowrap"><span className={`badge ${Number(v.stock) > 0 ? 'badge-primary':'badge-danger'}`}>{Number(v.stock) > 0 ? 'Stok Ada':'Stok Kosong'}</span></td>*/}
                {/*              </tr>*/}
                {/*          );*/}
                {/*        }):<tr><td colSpan={4} className={"text-center"}>Data Tidak Tersedia</td></tr>*/}
                {/*      }*/}

                {/*      </tbody>*/}
                {/*    </table>*/}

                {/*  </div>*/}
                {/*</div>*/}
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="card text-center">
                  <div className="card-body">
                    <h4 className="card-title">TOTAL PENJUALAN 1 Tahun Terakhir</h4>

                    {this.state.chartPenjualanBerdasarkanBulan!==null&&<Chart
                        options={this.state.chartPenjualanBerdasarkanBulan.options}
                        series={this.state.chartPenjualanBerdasarkanBulan.series}
                        type="bar"
                        height="300"
                    />}
                  </div>
                </div>
              </div>
              <div className="col-md-4 box-margin">
                <div className="card text-center">
                  <div className="card-body">
                    <h4 className="card-title">TOTAL PENJUALAN BULANAN</h4>
                    <Chart options={this.state.lokasi_sales.options} series={this.state.lokasi_sales.series} type="bar" height="300" />
                  </div>
                </div>
              </div>
              <div className="col-md-4 box-margin">
                <div className="card text-center">
                  <div className="card-body">
                    <h4 className="card-title">TOTAL TRANSAKSI BULANAN</h4>
                    <Chart options={this.state.lokasi_tr.options} series={this.state.lokasi_tr.series} type="bar" height="300" />
                  </div>
                </div>
              </div>
              <div className="col-md-4 box-margin">
                <div className="card">
                  <div className="card-header bg-transparent text-center">
                    <h4 className="card-title mt-3">TOTAL PENJUALAN MINGGU INI</h4>
                  </div>
                  <div className="card-body">
                    <Chart options={this.state.daily.options} series={this.state.daily.series} type="bar" height="300" />
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              {/* <div className="col-md-4 box-margin"> */}
              {/*   <div className="card"> */}
              {/*     <div className="card-header bg-transparent text-center"> */}
              {/*       <h4 className="card-title mt-3">TOTAL PENJUALAN MINGGU INI</h4> */}
              {/*     </div> */}
              {/*     <div className="card-body"> */}
              {/*       <Chart options={this.state.daily.options} series={this.state.daily.series} type="bar" height="300" /> */}
              {/*     </div> */}
              {/*   </div> */}
              {/* </div> */}
              {/* <div className="col-md-8 box-margin"> */}
              {/*   <div className="card"> */}
              {/*     <div className="card-header bg-transparent text-center"> */}
              {/*       <h4 className="card-title mt-3">HOURLY GROSS SALES AMOUNT</h4> */}
              {/*     </div> */}
              {/*     <div className="card-body"> */}
              {/*       <Chart options={this.state.hourly.options} series={this.state.hourly.series} height="300" /> */}
              {/*     </div> */}
              {/*   </div> */}
              {/* </div> */}
            </div>
            <div className="row">
              {/*<div className="col-md-6 box-margin">*/}
              {/*  <div className="card">*/}
              {/*    /!* <div className="card-body"> *!/*/}
              {/*    <Tabs>*/}
              {/*      <div className="card-header bg-transparent user-area d-flex align-items-center justify-content-between">*/}
              {/*        <h4 className="card-title mt-3">SUPPLIER TERBAIK</h4>*/}
              {/*        <TabList>*/}
              {/*          <Tab onClick={(e) => this.handleSelect3(e, 1)}>Volume</Tab>*/}
              {/*          <Tab onClick={(e) => this.handleSelect3(e, 2)}>Sales</Tab>*/}
              {/*        </TabList>*/}
              {/*      </div>*/}
              {/*      <div className="card-body">*/}
              {/*        <TabPanel>*/}
              {/*          <Chart options={this.state.top_sp_qty.options} series={this.state.top_sp_qty.series} type="bar" height="300" />*/}
              {/*        </TabPanel>*/}
              {/*        <TabPanel>*/}
              {/*          <Chart options={this.state.top_sp_sale.options} series={this.state.top_sp_sale.series} type="bar" height="300" />*/}
              {/*        </TabPanel>*/}
              {/*      </div>*/}
              {/*    </Tabs>*/}
              {/*    /!* </div> *!/*/}
              {/*  </div>*/}
              {/*</div>*/}

              {/* <div className="col-md-6 box-margin"> */}
              {/*   <div className="card"> */}
              {/*     /!* <div className="card-body"> *!/ */}
              {/*     <Tabs> */}
              {/*       <div className="card-header bg-transparent user-area d-flex align-items-center justify-content-between"> */}
              {/*         <h4 className="card-title mt-3">TOP 8 ITEMS</h4> */}
              {/*         <TabList> */}
              {/*           <Tab onClick={(e) => this.handleSelect(e, 1)}>Volume</Tab> */}
              {/*           <Tab onClick={(e) => this.handleSelect(e, 2)}>Sales</Tab> */}
              {/*         </TabList> */}
              {/*       </div> */}
              {/*       <div className="card-body"> */}
              {/*         <TabPanel> */}
              {/*           /!* <div className="card-body"> *!/ */}
              {/*           <Chart options={this.state.top_item_qty.options} series={this.state.top_item_qty.series} type="bar" height="600" /> */}
              {/*           /!* </div> *!/ */}
              {/*         </TabPanel> */}
              {/*         <TabPanel> */}
              {/*           <Chart options={this.state.top_item_sale.options} series={this.state.top_item_sale.series} type="bar" height="600" /> */}
              {/*         </TabPanel> */}
              {/*       </div> */}
              {/*     </Tabs> */}
              {/*     /!* </div> *!/ */}
              {/*   </div> */}
              {/* </div> */}
              <div className="col-md-12 box-margin">
                <div className="card">
                  <Tabs>
                    <div className="card-header bg-transparent user-area d-flex align-items-center justify-content-between">
                      <h4 className="card-title mt-3">KATEGORI TERLARIS</h4>
                      <TabList>
                        <Tab onClick={(e) => this.handleSelect2(e, 1)}>Volume</Tab>
                        <Tab onClick={(e) => this.handleSelect2(e, 2)}>Sales</Tab>
                      </TabList>
                    </div>
                    <div className="card-body">
                      <TabPanel>
                        <Chart options={this.state.top_cat_qty.options} series={this.state.top_cat_qty.series} type="bar" height="300" />
                        {/* </div> */}
                      </TabPanel>
                      <TabPanel>
                        <Chart options={this.state.top_cat_sale.options} series={this.state.top_cat_sale.series} type="bar" height="300" />
                      </TabPanel>
                    </div>
                  </Tabs>
                </div>
              </div>
            </div>
            <div className="row">
              {/* <div className="col-md-6 box-margin"> */}
              {/*   <div className="card"> */}
              {/*     /!* <div className="card-body"> *!/ */}
              {/*     <Tabs> */}
              {/*       <div className="card-header bg-transparent user-area d-flex align-items-center justify-content-between"> */}
              {/*         <h4 className="card-title mt-3">TOP 5 SUPPLIER</h4> */}
              {/*         <TabList> */}
              {/*           <Tab onClick={(e) => this.handleSelect3(e, 1)}>Volume</Tab> */}
              {/*           <Tab onClick={(e) => this.handleSelect3(e, 2)}>Sales</Tab> */}
              {/*         </TabList> */}
              {/*       </div> */}
              {/*       <div className="card-body"> */}
              {/*         <TabPanel> */}
              {/*           <Chart options={this.state.top_sp_qty.options} series={this.state.top_sp_qty.series} type="bar" height="300" /> */}
              {/*         </TabPanel> */}
              {/*         <TabPanel> */}
              {/*           <Chart options={this.state.top_sp_sale.options} series={this.state.top_sp_sale.series} type="bar" height="300" /> */}
              {/*         </TabPanel> */}
              {/*       </div> */}
              {/*     </Tabs> */}
              {/*     /!* </div> *!/ */}
              {/*   </div> */}
              {/* </div> */}

            </div>
          </div>

          {
              this.props.isOpen &&this.state.exportTopItem&&<DashboardExportExcel data={this.state.dataExportTopItem} periode={`${this.state.startDate} - ${this.state.endDate}`}/>
          }

        </Layout>
    );
  }
}
// Dashboard.propTypes = {
//     auth: PropTypes.object
// }

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    stock: state.dashboardReducer.data,
    isOpen: state.modalReducer,
    type: state.modalTypeReducer,
  };
};
export default connect(mapStateToProps)(Dashboard);
